﻿using HomeWork3_LINQ.Interfaces;
using HomeWork3_LINQ.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HomeWork3_LINQ.OperationType;

namespace HomeWork3_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            string strLogin = "";
            string strPass = "";
            int userId = 0;
            int i = 0;
            var usrRepo = new UserRepository();
            var accRepo = new AccountRepository();
            var opRepo = new OperationHistoryRepository();

            ATMManager atmManager = new ATMManager(usrRepo, accRepo, opRepo);

            // Users
            AddUsers(atmManager);
            Console.WriteLine("Users count: {0}", atmManager.CountUsers());

            // Accounts
            AddAccounts(atmManager);
            Console.WriteLine("Accounts count: {0}", atmManager.CountAccounts());

            // Operations
            AddOperations(atmManager);
            Console.WriteLine("Operations count: {0}", atmManager.CountOperations());

            // 1
            strLogin = "login1";
            strPass = "pass1";
            List<User> usrByLoginAndPass = (List<User>)atmManager.GetUserByLoginAndPass(strLogin, strPass);
            if (usrByLoginAndPass.Count == 0)
            {
                Console.WriteLine("Пользователей с логином '{0}' и паролем '{1}' не найдено", strLogin, strPass);
            }
            else
            {
                Console.WriteLine("Пользователи с логином '{0}' и паролем '{1}':", strLogin, strPass);
                foreach (var item in usrByLoginAndPass)
                {
                    Console.WriteLine("Имя пользователя: \t {0}", item.FirstName);
                    Console.WriteLine("Фамилия пользователя: \t {0}", item.LastName);
                    Console.WriteLine("Отчество пользователя: \t {0}", item.FathersName);
                }
            }

            // 2
            userId = 1;
            List<Account> usrAccounts = (List<Account>)atmManager.GetUserAccounts(userId);
            if (usrAccounts.Count == 0)
            {
                Console.WriteLine("У пользователя с номером '{0}' счетов не найдено", userId);
            }
            else
            {
                Console.WriteLine("Счета пользователя с номером '{0}':", userId);
                foreach (var item in usrAccounts)
                {
                    Console.WriteLine("Номер счета: N{0} {1}", ++i, item.AccountId);
                }
            }

            // 3
            userId = 1;
            var accOperations = atmManager.GetAccountsWithOperations(userId);
            if (accOperations.Count() == 0)
            {
                Console.WriteLine("У пользователя с номером '{0}' счетов не найдено", userId);
            }
            else
            {
                Console.WriteLine("Счета и операции пользователя с номером '{0}':", userId);
                foreach (var item in accOperations)
                {
                    Console.WriteLine("Номер счета N{0}: Номер операции: {1}, Тип операции: {2}, Сумма операции: {3}", item.Item2.AccountId, item.Item2.OperationId, ((OperationTypes)item.Item2.OperationType).ToString(), item.Item2.OperationAmount );
                }
            }

            // 4
            userId = 1;
            OperationTypes opType = OperationTypes.Credit;
            var creditOperations = atmManager.GetOperationsByType(opType);

            if (creditOperations.Count() == 0)
            {
                Console.WriteLine("Операций '{0}' не найдено", opType.ToString());
            }
            else
            {
                Console.WriteLine("Операции пополнения");
                foreach (var item in creditOperations)
                {
                    Console.WriteLine("Имя: {0}, Фамилия: {1}, Тип операции: {2}, Сумма операции: {3}, Дата операции: {4}", 
                        item.Item1.FirstName, item.Item1.LastName, ((OperationTypes)item.Item2.OperationType).ToString(), item.Item2.OperationAmount, item.Item2.OperationDate);
                }
            }

            // 5
            double sumLimit = 1000;
            var richUsers = atmManager.GetOperationsBySum(sumLimit);

            if (richUsers.Count() == 0)
            {
                Console.WriteLine("Пользователей, у которых на счету больше, чем '{0}' не найдено");
            }
            else
            {
                Console.WriteLine("Богатые пользоватлели:");
                foreach (var item in richUsers)
                {
                    Console.WriteLine("Имя: {0}, Фамилия: {1}, Номер счета: {2}, Баланс: {3}",
                        item.Item1.FirstName, item.Item1.LastName, item.Item2.AccountId, item.Item2.Balance.ToString());
                }
            }

            Console.ReadKey();
        }

        static void AddUsers(ATMManager atmManager)
        {
            atmManager.AddUser(new User
            {
                UserId = 1,
                FirstName = "Name1",
                LastName = "LastName1",
                FathersName = "FathersName1",
                Phone = "11223344",
                Passport = "AA012345678",
                RegistrationDate = new DateTime(1977, 01, 01),
                Login = "login1",
                Password = "pass1"
            });
            atmManager.AddUser(new User
            {
                UserId = 2,
                FirstName = "Name2",
                LastName = "LastName2",
                FathersName = "FathersName2",
                Phone = "44332211",
                Passport = "BB876543210",
                RegistrationDate = new DateTime(1977, 02, 02),
                Login = "login2",
                Password = "pass2"
            });
            atmManager.AddUser(new User
            {
                UserId = 3,
                FirstName = "Name3",
                LastName = "LastName3",
                FathersName = "FathersName3",
                Phone = "55667788",
                Passport = "CC876540123",
                RegistrationDate = new DateTime(1977, 03, 03),
                Login = "login3",
                Password = "pass3"
            });
        }

        static void AddAccounts(ATMManager atmManager)
        {
            atmManager.AddAccount(new Account
            {
                AccountId = 1111,
                RegistrationDate = new DateTime(2020, 09, 21),
                Balance = 1000,
                UserId = 1
            });
            atmManager.AddAccount(new Account
            {
                AccountId = 2222,
                RegistrationDate = new DateTime(2020, 09, 22),
                Balance = 2000,
                UserId = 1
            });
            atmManager.AddAccount(new Account
            {
                AccountId = 3333,
                RegistrationDate = new DateTime(2020, 09, 23),
                Balance = 3000,
                UserId = 2
            });
            atmManager.AddAccount(new Account
            {
                AccountId = 4444,
                RegistrationDate = new DateTime(2020, 09, 24),
                Balance = 4000,
                UserId = 2
            });
            atmManager.AddAccount(new Account
            {
                AccountId = 5555,
                RegistrationDate = new DateTime(2020, 09, 25),
                Balance = 5000,
                UserId = 3
            });
            atmManager.AddAccount(new Account
            {
                AccountId = 6666,
                RegistrationDate = new DateTime(2020, 09, 26),
                Balance = 6000,
                UserId = 3
            });
        }

        static void AddOperations(ATMManager atmManager)
        {
            atmManager.AddOperationHistory(new OperationHistory
            {
                OperationId = 111,
                OperationDate = new DateTime(2020, 09, 25),
                OperationType = (int)OperationTypes.Debet,
                OperationAmount = 1000,
                AccountId = 1111
            });
            atmManager.AddOperationHistory(new OperationHistory
            {
                OperationId = 112,
                OperationDate = new DateTime(2020, 09, 26),
                OperationType = (int)OperationTypes.Credit,
                OperationAmount = 5000,
                AccountId = 1111
            });
            atmManager.AddOperationHistory(new OperationHistory
            {
                OperationId = 113,
                OperationDate = new DateTime(2020, 09, 21),
                OperationType = (int)OperationTypes.Credit,
                OperationAmount = 5000,
                AccountId = 1111
            });
            atmManager.AddOperationHistory(new OperationHistory
            {
                OperationId = 114,
                OperationDate = new DateTime(2020, 09, 27),
                OperationType = (int)OperationTypes.Debet,
                OperationAmount = 10000,
                AccountId = 1111
            });
        }
    }
}
