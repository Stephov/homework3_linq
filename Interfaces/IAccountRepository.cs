﻿using HomeWork3_LINQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public interface IAccountRepository: IBaseRepository<Account>
    {
        void CheckAccountData(Account item);
        void ChangeBalance(OperationHistory item);
    }
}


