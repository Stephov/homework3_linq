﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public interface IATMManagerService
    {
        void AddOperationHistory(OperationHistory operation);
    }
}