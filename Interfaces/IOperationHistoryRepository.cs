﻿using HomeWork3_LINQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public interface IOperationHistoryRepository : IBaseRepository<OperationHistory>
    {
        void CheckOperationData(OperationHistory item);
    }
}


