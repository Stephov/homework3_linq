﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace HomeWork3_LINQ.Interfaces
{
    public interface IBaseRepository<T>
    {
        public void Add(T item);
        public T GetOne(Func<T, bool> predicate);
        public IEnumerable<T> GetAll(Func<T, bool> predicate = null);
        public int GetCount();
    }
}
