﻿using HomeWork3_LINQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeWork3_LINQ.Repositories
{
    class BaseRepository<T> : IBaseRepository<T>
    {
        private readonly List<T> _items = new List<T>();

        public void Add(T item)
        {
            _items.Add(item);
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate = null)
        {
            return predicate == null ?  _items.ToList() : _items.ToList().Where(predicate);
        }

        public int GetCount()
        {
            return _items.Count();
        }

        public T GetOne(Func<T, bool> predicate)
        {
            return GetAll().FirstOrDefault(a => predicate(a));
        }
    }
}
