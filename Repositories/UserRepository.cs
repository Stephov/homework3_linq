﻿using HomeWork3_LINQ.Interfaces;
using HomeWork3_LINQ.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    class UserRepository : BaseRepository<User>, IUserRepository
    {
        public void CheckUserData(User item)
        {
            if (item.UserId == 0)
            {
                throw new Exception("Укажите Номер пользователя");
            }

            IEnumerable<User> usrList = GetAll();
            foreach (var usr in usrList)
            {
                if (usr.UserId == item.UserId)
                {
                    throw new Exception("Пользователь с номером {0} " + usr.UserId.ToString() + " уже зарегистрирован");
                }
            }
            if (string.IsNullOrEmpty(item.FirstName))
            {
                throw new Exception("Укажите Имя пользователя");
            }
            if (string.IsNullOrEmpty(item.LastName))
            {
                throw new Exception("Укажите Фамилию пользователя");
            }
        }
    }
}
