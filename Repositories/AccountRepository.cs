﻿using HomeWork3_LINQ.Interfaces;
using HomeWork3_LINQ.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using static HomeWork3_LINQ.OperationType;

namespace HomeWork3_LINQ
{
    class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public void ChangeBalance(OperationHistory item)
        {
            Account acc = GetOne(x => x.AccountId == item.AccountId);
            if ((item.OperationType == (int)OperationTypes.Debet) && acc.Balance < item.OperationAmount)
            {
                throw new Exception("На счету недостаточно средств");
            }
            else
                acc.Balance = (item.OperationType == (int)OperationTypes.Debet) ? acc.Balance - item.OperationAmount : acc.Balance + item.OperationAmount;
        }

        public void CheckAccountData(Account item)
        {
            if (item.AccountId == 0)
            {
                throw new Exception("Укажите Номер счета");
            }

            IEnumerable<Account> accList = GetAll();
            foreach (var acc in accList)
            {
                if (acc.AccountId == item.AccountId)
                {
                    throw new Exception("Счет с номером " + acc.AccountId.ToString() + " уже зарегистрирован");
                }
            }
        }
    }
}
