﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ.Repositories
{
    class OperationHistoryRepository : BaseRepository<OperationHistory>, IOperationHistoryRepository
    {
        public void CheckOperationData(OperationHistory item)
        {
            if (item.OperationId == 0)
            {
                throw new Exception("Укажите Номер операции");
            }
            if (item.OperationAmount == 0)
            {
                throw new Exception("Укажите Сумму операции");
            }

            IEnumerable<OperationHistory> opList = GetAll();
            foreach (var op in opList)
            {
                if (op.OperationId == item.OperationId)
                {
                    throw new Exception("Операция с номером " + op.OperationId.ToString() + " уже зарегистрирована");
                }
            }
        }
    }
}

