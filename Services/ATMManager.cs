﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HomeWork3_LINQ.OperationType;

namespace HomeWork3_LINQ
{
    public class ATMManager
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOperationHistoryRepository _operationRepository;


        public ATMManager(IUserRepository userRepository, IAccountRepository accountRepository, IOperationHistoryRepository operationRepository)
        {
            _accountRepository = accountRepository;
            _userRepository = userRepository;
            _operationRepository = operationRepository;
        }

        public void AddUser(User user)
        {
            _userRepository.CheckUserData(user);
            _userRepository.Add(user);
        }

        public void AddAccount(Account account)
        {
            _accountRepository.CheckAccountData(account);
            _accountRepository.Add(account);
        }

        public void AddOperationHistory(OperationHistory operation)
        {
            _operationRepository.CheckOperationData(operation);
            _operationRepository.Add(operation);
            _accountRepository.ChangeBalance(operation);
        }

        public int CountUsers()
        {
            return _userRepository.GetCount();
        }

        public int CountAccounts()
        {
            return _accountRepository.GetCount();
        }

        public int CountOperations()
        {
            return _operationRepository.GetCount();
        }

        // Вывод информации о заданном аккаунте по логину и паролю
        public IEnumerable<User> GetUserByLoginAndPass(string login, string pass)
        {
            IEnumerable<User> retValue = _userRepository.GetAll(x => x.Login == login && x.Password == pass).ToList();

            return retValue;
        }

        // Вывод данных о всех счетах заданного пользователя;
        public IEnumerable<Account> GetUserAccounts(int userId)
        {
            IEnumerable<Account> retValue = _accountRepository.GetAll(x => x.UserId == userId).ToList();

            return retValue;
        }

        // Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        public IEnumerable<Tuple<Account, OperationHistory>> GetAccountsWithOperations(int userId)
        {

            var retValue = from a in _accountRepository.GetAll()
                   join o in _operationRepository.GetAll()
                   on a.AccountId equals o.AccountId
                   where a.UserId == userId
                   select (new Tuple<Account, OperationHistory>(a, o));

            return retValue;
        }

        // Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        public IEnumerable<Tuple<User, OperationHistory>> GetOperationsByType(OperationTypes opType)
        {
            var retValue = from a in _accountRepository.GetAll()
                           join o in _operationRepository.GetAll()
                           on a.AccountId equals o.AccountId
                           join u in _userRepository.GetAll()
                           on a.UserId equals u.UserId
                           where o.OperationType == (int)opType
                           select (new Tuple<User, OperationHistory>(u, o));

            return retValue;
        }

        // Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);
        public IEnumerable<Tuple<User, Account>> GetOperationsBySum(double dSum)
        {
            var retValue = from a in _accountRepository.GetAll()
                           join u in _userRepository.GetAll()
                           on a.UserId equals u.UserId
                           where a.Balance >= dSum
                           select (new Tuple<User, Account>(u, a));

            return retValue;
        }

    }
}


