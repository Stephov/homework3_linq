﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public class OperationHistory
    {
        public  int OperationId { get; set; }
        public DateTime OperationDate { get; set; }
        public int OperationType { get; set; }
        public double OperationAmount { get; set; }
        public int AccountId { get; set; }
    }
}
