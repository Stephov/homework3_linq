﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public class Account
    {
        public int AccountId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public double Balance { get; set; }
        public int UserId { get; set; }

    }
}
