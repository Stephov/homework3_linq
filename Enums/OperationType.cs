﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3_LINQ
{
    public class OperationType
    {
        public enum OperationTypes: int
        {
            [Description("Debet")]
            Debet = 0,
            [Description("Credit")]
            Credit = 1
        }
    }
}
